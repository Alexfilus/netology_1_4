const fs = require('fs');
const crypto = require('crypto');
const fileInput = '1.txt';
const fileOutput = 'res.txt';

const Convert = require('./convert').Convert;

let hash = crypto.createHash('md5');
let convertStream = new Convert();

let resHash = fs
    .createReadStream(fileInput)
    .pipe(hash)
    .pipe(convertStream);
resHash.pipe(fs.createWriteStream(fileOutput));
resHash.pipe(process.stdout);