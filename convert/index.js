const Transform = require('stream').Transform;
class Convert extends Transform {
    _transform(chunk) {
        this.push(chunk.toString('hex'));
    };
}
exports.Convert = Convert;